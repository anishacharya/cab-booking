const express = require('express');
const bodyParser = require('body-parser');
const rateLimit = require('express-rate-limit');
const swaggerUi = require('swagger-ui-express');
const usersRoutes = require('./routes/users');
const bookingRoutes = require('./routes/booking');
const cabsRoutes = require('./routes/cabs');
const database = require('./database');
const swaggerDocs = require('./swaggerDocs.json');
const cabs = require('./cabsLocation');

const app = express();
const PORT = 4000;
app.use(bodyParser.json());

const swaggerOptions = {
  swaggerDefinition: {
    openai: '3.0.0',
    info: {
      title: 'cabs api',
      description: 'A user can book a cab , view past booking and can get nearby cabs',
      contact: {
        name: 'aanishacharyaa@gmail.com',
      },
      servers: 'http://localhost:4000',
    },
  },
  apis: ['./routes/*'],
};

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs, swaggerOptions));

const userApiLimit = rateLimit({
  windowMs: 20 * 60 * 1000, // 20 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  message: 'Limit reached, please try again after 15 min',
});
const bookingApiLimit = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 300, // limit each IP to 300 requests per windowMs
  message: 'Limit reached, please try again after 15 min',
});

app.use((error, req, res, next) => {
  // Catch json error
  console.log(error);
  res.status(406).send('invalid json request');
  next();
});

app.use('/users', userApiLimit, usersRoutes);
app.use('/booking', bookingApiLimit, bookingRoutes);
app.use('/cabs', bookingApiLimit, cabsRoutes);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Sorry!Something broke!');
  next();
});

app.all('/*', (req, res) => {
  res.status(400).send('available end-points are /users, /booking and /cabs');
});

app.listen(PORT, () => {
  console.log(`up and running on port ${PORT}`);
  database.connectDB();
  cabs.getdata();
});
