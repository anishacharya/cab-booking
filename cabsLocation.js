const jsdom = require('jsdom');
const nodefetch = require('node-fetch');
const jq = require('jquery');

const { JSDOM } = jsdom;

const cabs = [];

const mainUrl = 'https://www.distancelatlong.com/country/india';
cabs.data = [];

cabs.getdata = () => {
  nodefetch(mainUrl)
    .then((res) => res.text())
    .then((body) => {
      const dom = new JSDOM(body);
      const $ = jq(dom.window);
      const urls = [];
      $('table.table.table-striped.setBorder > tbody').find('tr.setFont').each(function () {
        urls.push($(this).find('a').attr('href'));
      });
      return urls;
    })
    .then(async (urls) => {
      urls.forEach((url) => {
        nodefetch(url)
          .then((res) => res.text())
          .then((body) => {
            const dom = new JSDOM(body);
            const $ = jq(dom.window);

            $('table.table.table-striped.setBorder > tbody').find('tr.setFont').each(function () {
              let obj = [];
              $(this).find('td').each(function (index) {
                obj[index] = $(this).text();
              });

              if (obj.length === 4) {
                obj.pop();
                if (obj[0] != null && obj[0] !== '' && obj[0].length !== 0) {
                  cabs.data.push(obj);
                  console.log(`updating cabs data from ${obj[0]}`);
                }
              } else if (obj.length === 3) {
                if (obj[0] != null && obj[0] !== '' && obj[0].length !== 0) {
                  cabs.data.push(obj);
                  console.log(`updating cabs data from ${obj[0]}`);
                }
              }
              obj = [];
            });
          });
      });
    });
};

module.exports = cabs;
