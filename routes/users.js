const express = require('express');
const database = require('../database');

const router = express.Router();

router.get('/', (req, res) => {
  const queryobj = {};
  if (req.query) {
    if (req.query.limit) {
      queryobj.limit = parseInt(req.query.limit, 10);
    } else if (req.query.skip) {
      queryobj.offset = parseInt(req.query.skip, 10);
    }
  } else {
    queryobj.limit = 50; // by default limit to 10
  }

  database.getAllUsers(queryobj, res);
});

router.post('/', (req, res) => {
  const user = req.body;
  if (req.body.name && req.body.email && req.body.password) {
    database.createUser(user.name, user.email, user.password, res);
  } else {
    res.status(400).send("invaild 'name' or 'email' or 'password'");
  }
});

router.get('/:id', (req, res) => {
  const { id } = req.params;
  database.getById(id, res);
});

router.delete('/:id', (req, res) => {
  const { id } = req.params;
  database.deleteById(id, res);
});

router.patch('/:id', (req, res) => {
  if (req.body.name && req.body.email && req.body.password) {
    const { id } = req.params;
    const { name, email, password } = req.body;
    const tempObj = {};
    if (name) { tempObj.name = name; }
    if (email) { tempObj.email = email; }
    if (password) { tempObj.password = password; }
    database.patchById(id, tempObj, res);
  } else {
    res.status(400).send("missing 'name' or 'email' or 'password'");
  }
});

router.all('/*', (req, res) => {
  res.status(400).send('only /users available');
});

module.exports = router;
