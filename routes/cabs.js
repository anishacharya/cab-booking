const express = require('express');
const database = require('../database');

const router = express.Router();

router.get('/:id', (req, res) => {
  const { id } = req.params;
  let limit = 5;
  if (req.query.limit) {
    limit = req.query.limit;
  }
  if (req.query.currentCity) {
    const { currentCity } = req.query;
    database.getNearby(true, id, currentCity, limit, res);
  } else if (req.query.longitude && req.query.latitude) {
    const { longitude, latitude } = req.query;
    database.getNearby(false, id, { longitude, latitude }, limit, res);
  } else {
    res.status(400).send('missing currentCity');
  }
});

router.all('/*', (req, res) => {
  res.status(400).send("only path '/cabs' available");
});

module.exports = router;
