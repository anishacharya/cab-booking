const express = require('express');
const database = require('../database');

const router = express.Router();

router.post('/:id', (req, res) => {
  if (req.body.from && req.body.to) {
    const { id } = req.params;
    const { from, to } = req.body;
    const obj = {};
    obj.from = from;
    obj.to = to;
    database.book(id, obj, res);
  } else {
    res.status(400).send("missing 'from' or 'to'");
  }
});

// pagination available using limit and skip
router.get('/:id', (req, res) => {
  const { id } = req.params;
  const queryobj = {};
  if (req.query) {
    if (req.query.limit) {
      queryobj.limit = parseInt(req.query.limit, 10);
    }
    if (req.query.skip) {
      queryobj.offset = parseInt(req.query.skip, 10);
    }
  } else {
    queryobj.limit = 50; // by default limit to 50
  }

  database.getAllBookingsForUser(id, queryobj, res);
});

router.all('/*', (req, res) => {
  res.status(400).send('only /booking available');
});

module.exports = router;
