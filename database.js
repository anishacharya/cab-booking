const Sequelize = require('sequelize');
const { v4: uuidv4 } = require('uuid');
const WorldCities = require('worldcities');
const distFrom = require('distance-from');
const cabs = require('./cabsLocation');
const config = require('./config/config.json');

const database = {};
const sequelize = new Sequelize(`${config.development.dialect}://${config.development.username}:${config.development.password}@${config.development.host}:3306/${config.development.database}`);

const user = sequelize.define('users', {
  id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV4, primaryKey: true },
  name: { type: Sequelize.STRING, allowNull: false },
  email: { type: Sequelize.STRING, allowNull: false },
  password: { type: Sequelize.STRING, allowNull: false },
}, {
  timestamps: false,
});

const bookings = sequelize.define('bookings', {
  id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true },
  userid: { type: Sequelize.UUID, allowNull: false, primaryKey: false },
  to: { type: Sequelize.STRING, allowNull: false },
  from: { type: Sequelize.STRING, allowNull: false },
}, {
  timestamps: false,
});

database.connectDB = () => {
  console.log('connecting to db');
  sequelize
    .authenticate()
    .then(() => {
      console.log('db connection success!');
      sequelize.sync();
      // sequelize.sync({ force: true })
    })
    .catch((err) => {
      console.error(err);
      database.connectDB();
    });
};

database.getAllUsers = (queryobj, res) => {
  const query = queryobj;
  query.attributes = ['id', 'name', 'email'];
  user.findAll(queryobj)
    .then((data) => {
      if (data === null) {
        res.send('no users found');
      } else if (data.length === 0) {
        res.send('there are currently no users');
      } else {
        res.send(data);
      }
    });
};

database.createUser = (name, email, password, res) => {
  user.findOne({ where: { email } })
    .then((queryemail) => {
      if (queryemail === null) {
        const id = uuidv4();
        const newUser = user.build({
          id, name, email, password,
        });
        newUser.save()
          .then((resp) => {
            const text = '\nnote down your id as it will be required to use other apis';
            res.send(`your id is : ${resp.id}${text}`);
          })
          .catch((err) => {
            console.error(err);
            res.status(500).end();
          });
      } else {
        res.send('email already used');
      }
    });
};

database.getById = (id, res) => {
  user.findOne({ where: { id } })
    .then((qid) => {
      if (qid === null) {
        res.status(404).send('User not found');
      } else {
        res.send(qid);
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
};

database.deleteById = (id, res) => {
  user.findOne({ where: { id } })
    .then((qid) => {
      if (qid === null) {
        res.status(404).send('no such user with that id exist');
      } else {
        user.destroy({
          where: {
            id,
          },
        })
          .then(() => {
            res.send(`user with id ${id} deleted`);
          })
          .catch((err) => {
            console.error(err);
            res.status(500).end();
          });
      }
    });
};

database.patchById = (id, updateobj, res) => {
  user.findOne({ where: { id } }).then((qid) => {
    if (qid === null) {
      res.status(400).send('please register first');
    } else {
      user.update(updateobj, {
        where: {
          id,
        },
      }).then(() => {
        res.send(`updated details for id ${id}`);
      }).catch((err) => {
        console.error(err);
        res.status(500).end();
      });
    }
  });
};

database.book = (id, dataobj, res) => {
  user.findOne({ where: { id } }).then((qid) => {
    if (qid === null) {
      res.status(400).send('please register first');
    } else {
      // booking
      const obj = dataobj;
      obj.userid = id;
      const newbooking = bookings.build(obj);
      newbooking.save()
        .then(() => {
          res.send(`booking done! from ${obj.from} to ${obj.to} for id ${id}`);
        })
        .catch((err) => {
          console.error(err);
          res.status(500).end();
        });
    }
  })
    .catch((err) => {
      console.error(err);
    });
};

database.getAllBookings = (res) => {
  bookings.findAll({ attributes: ['from', 'to'] })
    .then((data) => {
      if (data === null) {
        res.send('no bookings found ');
      } else if (data.length === 0) {
        res.send('no bookings done yet ');
      } else {
        res.send(data);
      }
    }).catch((err) => {
      console.error(err);
      res.status(500).end();
    });
};

database.getAllBookingsForUser = (userid, obj, res) => {
  user.findOne({ where: { id: userid } })
    .then((qid) => {
      if (qid === null) {
        res.send(`no user with id ${userid}`);
      } else {
        const queryobj = obj;
        queryobj.where = { userid };
        queryobj.attributes = ['from', 'to'];
        bookings.findAll(queryobj)
          .then((data) => {
            if (data === null) {
              res.send(`no bookings done by user with id ${userid}`);
            } else if (data.length === 0) {
              if (queryobj.offset) {
                res.send('no more.. you may reset skip');
              }
            } else {
              res.send(data);
            }
          }).catch((err) => {
            res.status(500).end();
            console.error(err);
          });
      }
    });
};

database.getNearby = (isCity, userid, cityOrObj, limit, res) => {
  user.findOne({ where: { id: userid } })
    .then((qid) => {
      if (qid === null) {
        res.send(`no user with id ${userid}`);
      } else {
        let userlatitude = null;
        let userlongitude = null;

        if (isCity) {
          const city = WorldCities.getByName(cityOrObj);
          if (city) {
            userlatitude = city.latitude;
            userlongitude = city.longitude;
          } else {
            res.status(400).send('please recheck city name');
          }
        } else {
          userlatitude = cityOrObj.latitude;
          userlongitude = cityOrObj.longitude;
        }

        const calData = [];

        for (let i = 0; i < cabs.data.length; i += 1) {
          const cabData = cabs.data[i];
          const cabLocation = cabData[0];
          const cablatitude = cabData[1];
          const cablongitude = cabData[2];

          const distance = distFrom([userlatitude, userlongitude]).to([cablatitude, cablongitude]).in('km');

          calData.push([`cab nearby in ${cabLocation}`, (~~distance)]);
        }

        calData.sort((a, b) => a[1] - b[1]);
        calData.length = limit;
        for (let i = 0; i < calData.length; i += 1) {
          calData[i][1] = `${calData[i][1]} km`;
        }

        res.send(calData);
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
};

module.exports = database;
